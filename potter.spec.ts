const BOOK_PRICE = 800;

enum Book {
    FIRST,
    SECOND,
    THIRD,
}

function discount(livres: Book[]) {
    switch (new Set(livres).size) {
        case 3:
            return 0.10;
        case 2:
            return 0.05;
        default:
            return 0.00;
    }
}

function price(books: Book[]): number {
    return BOOK_PRICE * books.length * (1 - discount(books))
}

describe('Potter', () => {
    describe('Pas de réduction', () => {
        it('un livre seul coute 8 balles', () => {
            expect(price([Book.FIRST])).toEqual(800)
        });

        it('acheter deux fois le même tome ne donne pas de réduction', () => {
            expect(price([Book.FIRST, Book.FIRST])).toEqual(1600)
        });
    })

    describe('Avec réduction', () => {
        it('acheter deux tomes donne 5% réduction', () => {
            expect(price([Book.FIRST, Book.SECOND])).toEqual(1600 * 0.95)
        })

        it('acheter trois tomes donne 10% réduction', () => {
            expect(price([Book.SECOND, Book.FIRST, Book.THIRD])).toEqual(3 * 800 * 0.90)
        });
    })
})
